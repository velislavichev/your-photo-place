<?php

use App\Http\Controllers\PhotoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Photos landing page
Route::get('/', [PhotoController::class, 'index'])->name('home');

// Individual photo page
Route::get('/{photo}', [PhotoController::class, 'show']);
