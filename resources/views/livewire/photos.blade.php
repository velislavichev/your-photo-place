<div>

    <h1 class="text-3xl text-gray-800 font-bold mb-12">
        Photos
    </h1>

    <div
        class="grid grid-cols-1 p-4 sm:p-0 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-4 gap-6 text-center">

        @forelse ($photos as $photo)

            <a wire:click="deleteImage({{ $photo['id'] }}, {{ $photo['deleted'] }})" x-data="{ show: false }" @click.prevent="show = !show"
                :class="{ 'hidden': show }" 
                class="{{ $photo['deleted'] ? 'cursor-not-allowed opacity-25' : 'hover:opacity-75' }}" href="#">

                <picture>
                    <source media="(min-width: 640px)" srcset="{{ $photo['url'] }}">
                    <img class="m-auto" src="{{ $photo['thumbnailUrl'] }}" />
                </picture>

            </a>

        @empty
            <h1>
                Sorry, no results found.
            </h1>
        @endforelse

    </div>

    <div class="w-full text-center">
        <button wire:click="loadMore" class="w-48 m-6 p-2 rounded text-white font-semibold bg-blue-500">
            Load more
        </button>
    </div>

    <!-- Loader -->
    <div wire:loading wire:target="loadMore" class="w-full h-full fixed block top-0 left-0 bg-white opacity-75 z-50">
    </div>



</div>
