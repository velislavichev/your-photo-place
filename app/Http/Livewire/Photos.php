<?php

namespace App\Http\Livewire;

use App\Models\Photo;
use App\Traits\ManagePhoto;
use Livewire\Component;

class Photos extends Component
{
    use ManagePhoto;

    /**
     * 
     * @var array
     */
    public $photos = [];

    /**
     *
     * @var integer
     */
    public $perPage = 30;

    /**
     * Load more
     *
     * @return void
     */
    public function loadMore()
    {
        $this->perPage += 30;
    }

    /**
     * Create new record in photos table so later we can determine which has been deleted
     *
     * @param int $id
     * @param bool $deleted
     * @return void
     */
    public function deleteImage($id, $deleted = false)
    {
        // Prevent delete on already delete images
        // if ($deleted) {
        //     return;
        // }

        // Delete image functionality
        $photo = collect($this->getCachedPhoto($id))->except(['id', 'albumId'])->put('photo_id', $id);

        Photo::create($photo->toArray());

        // Add new img to grid after deleting
        $this->perPage = $this->perPage + 1;
    }

    /**
     * Return photos component
     *
     * @return view
     */
    public function render()
    {
        $this->photos = $this->getPhotos();

        return view('livewire.photos', [
            'photos' => $this->photos
        ]);
    }
}
