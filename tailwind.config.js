module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
    './app/**/*.php',
    './resources/**/*.php',
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
