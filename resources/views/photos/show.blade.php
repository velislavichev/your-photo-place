@extends('layout.app')

@section('content')

    <h1 class="text-3xl text-gray-800 font-bold mb-12">
        {{ $photo->title }}
    </h1>

    <a class="pointer w-auto h-auto" href="#">
        <picture>
            <source media="(min-width: 640px)" srcset="{{ $photo->url }}">
            <img class="m-auto" src="{{ $photo->thumbnailUrl }}" />
        </picture>
    </a>

    <div class="mt-8">
        <a href="{{ route('home') }}" class="w-48 p-2 rounded text-white font-semibold bg-blue-500">
            Back to Photos
        </a>
    </div>

@endsection
