<?php

namespace App\Traits;

use App\Models\Photo;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

trait ManagePhoto
{

    /**
     * Format photos data for liveware component view
     *
     * @param  array $photos
     * @return collection
     */
    private function formatPhotos($photos)
    {
        // Get deleted photos ids
        $deletedPhotosIds = Photo::pluck('photo_id')->all();

        return collect($photos)->map(function ($photo) use ($deletedPhotosIds) {
            return collect($photo)->merge([
                'deleted' => in_array($photo['id'], $deletedPhotosIds)
            ]);
        })->take($this->perPage)->toArray();
    }

    /**
     * Get photos from the api
     *
     * @return array
     */
    private function getPhotos()
    {
        // Since there is no filter to take specific number of photoes built into the api,
        // cache data for a day to improve loading time
        $photos = Cache::remember('photos', 86400, function () {
            return Http::get('https://jsonplaceholder.typicode.com/photos')->json();
        });

        // Check if array is empty - photos not found
        abort_if(!$photos, 404);

        return $this->formatPhotos($photos);
    }


    /**
     * Get single photo from the cache based on photo id
     *
     * @param int $id
     * @return array/null
     */
    private function getCachedPhoto($id)
    {
        if (Cache::has('photos')) {
            $photos = Cache::get('photos');

            foreach ($photos as $photo) {
                if ($photo['id'] === $id) {
                    return $photo;
                }
            }

            return null;
        }
    }
}
