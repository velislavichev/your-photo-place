<?php

namespace App\Http\Controllers;

use App\Models\Photo;

class PhotoController extends Controller
{
    /**
     * Main photos view
     *
     * @return view
     */
    public function index()
    {
        return view('photos.index');
    }

    /**
     * Single photo view
     *
     * @return view
     */
    public function show(Photo $photo)
    {
        return view('photos.show', compact('photo'));
    }
}
